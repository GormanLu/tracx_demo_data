% Makes sure paths to the data and scripts are set correctly
run('setup_environment.m')

%% Load existing Project
Tracker = TracX.Tracker();
Tracker.loadTrackingProject(demoScriptPath, 'TracX_Yeast_SCerevisiae_Experiment_Project.xml') 

%% Make sure paths are set correctly
run('setup_environment.m')

%% Initialize Tracker
Tracker = TracX.Tracker();
Tracker.loadTrackingProject(demoScriptPath, 'TracX_Yeast_SCerevisiae_Experiment_Project') 

%% Change file paths to match your system
Tracker.configuration.ProjectConfiguration.setImageDir(fullfile(demoDataPath, 'yeast', 'scerevisiae', 'RawImages'));
Tracker.configuration.ProjectConfiguration.setSegmentationResultDir(fullfile(demoDataPath, 'yeast', 'scerevisiae', 'CellXSegmentation', 'mKO_mKate'));
Tracker.configuration.ProjectConfiguration.setTrackerResultsDirectory(fullfile(demoDataPath, 'yeast', 'scerevisiae', 'CellXSegmentation', 'mKO_mKate')) 
Tracker.data.clearAllData();
Tracker.importData();

%% Run the tracker
Tracker.runTracker()