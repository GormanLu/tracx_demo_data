%% Make sure paths are set correctly
run('setup_environment.m')

%% To track a generic segmentation mask (single page tiff file with pixel 
% belonging to a cell labeled with a increasing number or binary mask) we 
% first have to convert the data and quantify potential signals.
% Note: this step will require the parallel computing toolbox and take some
% time to quantify all images.
Tracker = TracX.Tracker();
imagePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'RawImages'); % Path to raw images.
segmentationPath = fullfile(demoDataPath,  'yeast', 'scerevisiae', 'GenericSegmentation'); % Path to segmentation masks.
segmentationFileNameRegex = 'mask_position1520600_*'; % File identifier for the segmentation masks
imageFileNameRegex = 'BFdivide_*'; % The Brightfield or phase contrast image.
fluoTags = {'mKO', 'mKate'}; % The channels for signal quantification
Tracker.prepareDataFromSegmentationMask(imagePath, ...
    segmentationPath, segmentationFileNameRegex, ...
    imageFileNameRegex, 'fluoTags', fluoTags)
clear Tracker

%%
% Configure a new tracking project
projectName = 'TracX_Yeast_SCerevisiae_Experiment'; % Project name
fileIdentifierFingerprintImages = 'BFdivide'; % Image identifier for Brigthfield images;
fileIdentifierWellPositionFingerprint = []; % Well position identifier if multiwell experiment.
fileIdentifierCellLineage = 'mKate'; % Image identifier for the Cell Lineage reconstruction (i.e bud neck marker).
imageCropCoordinateArray = []; % Empty if no crop has been applied in CellX, add CellX cropRegionBoundaries coordinates otherwise (from CellX_SCerevisiae_Parameter.xml).
movieLength = 30; % Number of timepoints to track 
cellsFilePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'GenericSegmentation'); % Path to segmentation results (CellX Style).
imagesFilePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'RawImages'); % Path to raw images.
cellDivisionType = 'asym'; % Cell division type.

% Create a tracker instance and a new project
Tracker = TracX.Tracker();
Tracker.createNewTrackingProject(projectName, imagesFilePath, ...
        cellsFilePath, fileIdentifierFingerprintImages, ...
        fileIdentifierWellPositionFingerprint, fileIdentifierCellLineage, ...
        imageCropCoordinateArray, movieLength, cellDivisionType);
Tracker.revertSegmentationImageCrop() % Reverts the potential image crop applied by CellX to map the cell coordinates to the full image.

%% Test default tracking parameters
% Optionally tune tracking parameters for better results
% Tracker.configuration.ParameterConfiguration.setMaxTrackFrameSkipping(3);
% Tracker.configuration.ParameterConfiguration.setMaxCellSizeDecrease(0.6);
% Tracker.configuration.ParameterConfiguration.setMaxCellCenterDisplacement(25)
Tracker.configuration.ParameterConfiguration.setMaxCellSizeIncrease(4)

% Dry run to test the tracking parameters
Tracker.testTrackingParameters([1, 30]) % Track from frame 1 to frame 30 for testing.

%% Run the tracker
Tracker.configuration.ParameterConfiguration.setMaxCellSizeIncrease(4)
Tracker.runTracker()

%% Save the tracking results
Tracker.saveCurrentTrackerState() % Saves the tracker state as mat file (to continue work anytime later)
Tracker.saveTrackingProject() % Saves the tracking project.
Tracker.saveTrackerResultsAsTable() % Saves the tracking results as one column seperated table for further analysis.
%%
Tracker.saveTrackerProjectControlImages('isParallel', true, 'maxWorkers', 12) % Save additional control images to inspect the sucess of the tracking.

%% Annotate a given image frame with any data property
% I.e for frame 15 we want to annotate the cell area. Skipping the property
% argument takes the track_index by default to inspect how well the
% tracking went.
Tracker.imageVisualization.plotAnnotatedControlImageFrame(15, 'cell_area')

%% Run optional lineage reconstruction

% Tune the sensitivity threshold until you only detect the true budnecks
% This tests the divisionMarkerEdgeSensitivityThresh=0.14 for frame 15 and
% will show 4 control images. Other parameters key value pairs that can be 
% modified are:
%    'divisionMarkerEdgeSensitivityThresh', 25, 
%    'divisionMarkerConvexAreaLowerThresh', 100,
%    'divisionMarkerConvexAreaUpperThresh', 5,
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMeanObjDiameter(7)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxExpectedMovement(5)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjCenterDisplacement(5)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMeanObjDiameterScalingFactor(1);
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxTrackFrameSkipping(1)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerEdgeSensitivityThresh(0.15) 
%Tracker.testBudNeckSegmentation(22, 'divisionMarkerEdgeSensitivityThresh', 0.15)

% Apply the relevant parameters from the test outcome
Tracker.configuration.ParameterConfiguration.setDivisionMarkerEdgeSensitivityThresh(0.15);
%% Test marker tracking parameters
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxExpectedMovement(6)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjCenterDisplacement(6)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeDecrease(0.4);
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeIncrease(4);

Tracker.testMarkerTrackingParameters([1, 30], 'divisionMarkerMaxTrackFrameSkipping', 1)
%%
% Indicate the cell division type, if optional control images should be
% saved and which channel bears the bud neck marker
% Apply the relevant parameters from the test outcome
Tracker.configuration.ParameterConfiguration.setDivisionMarkerEdgeSensitivityThresh(0.15);
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeDecrease(0.4);
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeIncrease(4);
Tracker.runLineageReconstruction('symmetricalDivision', false, ...
    'WriteControlImages', false, 'nuclearMarkerChannel', 1, 'divisionMarkerMaxTrackFrameSkipping', 1);
%% 
% Plot linaege for track 1 to console
trackToDisplay = 3;
Tracker.imageVisualization.plotLineageTree(trackToDisplay);
% Plot linaege for track 1 to figure
fh = Tracker.imageVisualization.plotLineageTree(trackToDisplay, 'plotToFigure', true);
fh{trackToDisplay}.Visible = true;

%% Print the cell cycle phase information as table and save it
Tracker.lineage.cellCyclePhaseTable
Tracker.saveTrackerCellCycleResultsAsTable()

%% Generate an animated movie of a lineage of interest
Tracker.imageProcessing.generateLineageMovie(3, 5/60, 'Track3LineageMovie')

%% Save  marker time series control images for lineage roots
% This creates a mask overlay with the budneck marker channel and nuclear
% channel signal
Tracker.imageProcessing.generateMarkerTimeSeriesImage('cycleMarker', ...
    {'mKOk', 'mKate'}, 'isPlotMarkerSignal', true)